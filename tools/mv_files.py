import os
import shutil


path_to_dir = '/tmp/fisher_out/'
out_dir = '/tmp/fisher_res/'
if not os.path.exists(out_dir):
	os.mkdir(out_dir)
dirs = os.listdir(path_to_dir)

for d in dirs:
	fams_p = path_to_dir+d+'/'
	out_fams_p = out_dir+d+'/'
	if not os.path.exists(out_fams_p):
		os.mkdir(out_fams_p)
	sets = os.listdir(fams_p)	
	pdfs = [l for l in sets if l.endswith('.res.txt') or
				l.endswith('.pdf') or l.endswith('.pkl')]
	for p in pdfs:
		shutil.copy(fams_p+p, out_fams_p+p)
		#os.remove (fams_p+p)

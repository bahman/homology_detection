'''
Created on 6 Aug 2011

@author: bahman
'''
#!/usr/bin/env python

import os
import sys
import traceback
import getpass
import pprint
import random
from time import sleep
from threading import Thread
from subprocess import Popen, PIPE

#from ssh_connection import SSHConnection
from g_mr import SSHWorker, ssh_workers, WorkerStopToken, LocalWorker
if(sys.hexversion < 0x03000000):
	import Queue
else:
	import queue as Queue


# svmtrain and gnuplot executable
is_win32 = (sys.platform == "win32")
if not is_win32:
	svmtrain_exe = "/home/ba2g09/workspace/homology_detection/libsvm-3.1/svm-train"
else:
	# example for windows
	svmtrain_exe = r"..\windows\svm-train.exe"

# global parameters and their default values
fold = 5
ds_paths = []
c, g = 2.0, 0.2
global pass_through_string
global out_filename


# process command line options, set global parameters
def process_options(argv=sys.argv):

	global fold
	global c, g
	global pass_through_string
	global svmtrain_exe, ds_paths, out_filename
	
	usage = """
		Usage: grid.py [-c c] [-g g] [-v fold] 
		[-svmtrain pathname] [-out pathname] [-png pathname]
		[additional parameters for svm-train] -f datasets
		"""

	if len(argv) < 2:
		print(usage)
		sys.exit(1)

	out_filename = "datasets_run.out"
	pass_through_options = []
	are_files = False

	i = 1
	while i < len(argv) :
		if are_files:
			assert os.path.exists(argv[i]),"dataset not found"
			ds_paths.append(argv[i])
		elif argv[i] == "-v":
			i = i + 1
			fold = argv[i]
		elif argv[i] == "-g":
			i = i + 1
			g = argv[i]
		elif argv[i] == "-c":
			i = i + 1
			c = argv[i]
		elif argv[i] == "-svmtrain":
			i = i + 1
			svmtrain_exe = argv[i]
		elif argv[i] == "-out":
			i = i + 1
			out_filename = argv[i]
		elif argv[i] == "-f":
			are_files = True
		else:
			pass_through_options.append(argv[i])
		i = i + 1

	pass_through_string = " ".join(pass_through_options)
	print svmtrain_exe
	assert os.path.exists(svmtrain_exe),"svm-train executable not found"	
	if len(ds_paths) == 0:
		raise RuntimeError("The ds_paths cannot be empty.")


def calculate_jobs():
	global ds_paths
	jobs = []
	for ds_p in ds_paths:	
		if ds_p.endswith('.tar.gz'):
			tars = 'tar -xf {0} -C /tmp/;'.format(ds_p)
			cmdstr = tars + '{0} -c {1} -g {2} -v {3} {4} /tmp/{5} > {6}; echo FINISHED'
			cmdline = (cmdstr.format(svmtrain_exe, c, g, fold, pass_through_string, 
						ds_p[:-7], ds_p+'.out'))
		else:
			cmdstr = '{0} -c {1} -g {2} -v {3} {4} {5} > {6}; echo FINISHED'
			cmdline = (cmdstr.format(svmtrain_exe, c, g, fold, pass_through_string, 
						ds_p, ds_p+'.out'))
		jobs.append(cmdline)	
	return jobs


def main():
	global ssh_workers
	# set parameters
	process_options()
	# put jobs in queue
	total_jobs = 0
	jobs = calculate_jobs()
	job_queue = Queue.Queue(0)
	result_queue = Queue.Queue(0)

	for cmdline in jobs:
		total_jobs += 1
		job_queue.put(cmdline)
	# hack the queue to become a stack --
	# this is important when some thread
	# failed and re-put a job. It we still
	# use FIFO, the job will be put
	# into the end of the queue, and the graph
	# will only be updated in the end
 
	job_queue._put = job_queue.queue.appendleft

	'''
	#Connect to hosts first
	hostnames = set(ssh_workers) #removing the extras
	hostnames = list(hostnames)
	#ssh_connector = SSHConnection(hostnames_l=hostnames)
	#ssh_connector.conn_to_clients_wait()
	#con_names = [c.hostname for c in ssh_connector.con_clients_l] 
	#connected_ones = [name.hostname  for name in ssh_connector.con_clients_l]
	#ssh_workers = []
	#for name in connected_ones:
		#ssh_workers.append(name)
		#ssh_workers.append(name)
		#ssh_workers.append(name)
		#ssh_workers.append(name)
		#ssh_workers.append(name)
	#print len(ssh_workers)
	#random.shuffle(ssh_workers)
	
	# fire ssh workers
	if ssh_workers:
		for host in ssh_workers:
			SSHWorker(host,job_queue,result_queue,host).start()
	'''

	# fire local workers
	nr_local_worker = 7
	for i in range(nr_local_worker):
		LocalWorker('local' + str(i), job_queue, result_queue).start()

	# gather results
	done_jobs = {}


	result_f = open(out_filename, 'w')

	for cmdline in jobs:
		while cmdline not in done_jobs:
			(worker, suc_run) = result_queue.get()
			done_jobs[cmdline] = suc_run
			result_f.write('{0} {1}\n'.format(cmdline, suc_run))
			result_f.flush()
			nr_done_jobs = len(done_jobs)
			perc = nr_done_jobs * 100.0 / total_jobs
			perc = round(perc,3)
			state = (str(nr_done_jobs) + " out of " + str(total_jobs) + 
						" " + str(perc)+"%")
			result = '{0} {1}\n'.format(cmdline, suc_run)
			print(result + " " + state)

	job_queue.put(WorkerStopToken)
	print 'Finished all'
	#ssh_connector.close_clients()

if __name__ == '__main__':
	#sys.argv.append('../very_small.txt')
	#print sys.argv
	main()
	#process_options()
	#print fold
	#print c, g
	#print svmtrain_exe, out_filename
	#pprint.pprint(ds_paths)
	#pprint.pprint(calculate_jobs())
	#jobs = calculate_jobs()
	#import pprint
	#pprint.pprint(jobs)

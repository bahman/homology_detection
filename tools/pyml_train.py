import os
import pickle
import pprint
from multiprocessing import Manager, Process, managers, Pool

import numpy as np
from PyML import *
from PyML.containers.vectorDatasets import SparseDataSet

from feat_sel import rank_feat


path_to_dir = '/tmp/fisher_out/'
dirs = os.listdir(path_to_dir)

arr = []
amino_grp = '20'
res_p = '/tmp/res/'+amino_grp+'.txt'

for d in  dirs:
	path_to_sets = path_to_dir+d
	data = os.listdir(path_to_sets)

	train_d = [p for p in data if p.endswith('_'+amino_grp+'.train')]
	test_d = [p for p in data if p.endswith('_'+amino_grp+'.test')]
	for i in range(len(train_d)):
		train_d[i] = path_to_sets + '/' + train_d[i]
		test_d[i] = path_to_sets + '/'+ test_d[i]
	train_d.sort()
	test_d.sort()
	for i in range(len(train_d)):
		arr.append((train_d[i], test_d[i], res_p))


def train_test(args):
	train_d, test_d, res_p = args[0], args[1], args[2]
	data = SparseDataSet(train_d)
	data.attachKernel('gaussian', gamma=0.2, C=2.0)

	##################################################
	#### This part of the code does Does statistical 
	#### feature selection ...
	labels = np.array([int(n) for n in data.labels.L])
	ranks = rank_feat(data.getMatrix().T, labels)
	ranks = [(abs(r),i) for i, r in enumerate(ranks)]
	ranks.sort()
	ranks.reverse()
	feats = [f[1] for f in ranks]
	data.keepFeatures(feats[:1000])

	s = svm.SVM()
	s.train(data)
	test = SparseDataSet(test_d)

	test.keepFeatures(feats[:1000])

	results = s.test(test)
	print(train_d, test_d, results.getROC())
	f = open (res_p, 'a')
	confMatrix = results.getConfusionMatrix()
	f.write(
			train_d.split('/')[-1] +' '+
			test_d.split('/')[-1]+' '+ 
			' TN ' + str(confMatrix[0][0]) + ' ' +
			' FN ' + str(confMatrix[0][1]) + ' ' +
			' FP ' + str(confMatrix[1][0]) + ' ' +
			' TP ' + str(confMatrix[1][1]) + ' ' +
			str(results.getROC())+'\n')
	f.close()
	#results.plotROC(test_d+'.pdf')
	results.toFile(test_d+'.res.txt')
	print (test_d)
	o = open(test_d+'.pkl', 'wb')
	pickle.dump(results, o)
	o.close()

pool = Pool(5)
pool.map(train_test, arr) 
#pprint.pprint (arr)

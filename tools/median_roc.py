import os
import pickle
import numpy as np
import matplotlib.pyplot as plt

#############################################################
# This file reads the pickle files generated by pyml and gets
# the ROC value at each given cordinat and then plots a graph

path_to_dir = '/tmp/fisher_res/'
path_to_dir = '/tmp/fisher_out/'
files_p = ['2.test.pkl', '4.test.pkl', '7.test.pkl', '10.test.pkl', '15.test.pkl', '20.test.pkl']
grh_l = ['--o', '-.^', '--<', '-.*', ':>', '-s']
files_p = []
grh_l = []
#files_p = ['2.test.pkl', '10.test.pkl', '15.test.pkl', '20.test.pkl']
#grh_l = ['--o', '-.^', '--<', '-.*']
dirs = os.listdir(path_to_dir)
graph_data = {}
for i, file_p in enumerate(files_p):
	print ("Doing :", file_p)
	fams = {}
	for d in dirs:
		fams[d] = []
		fams_p = path_to_dir+d+'/'
		sets = os.listdir(fams_p)	
		pkls = [l for l in sets if l.endswith(file_p)]
		for pkl_p in pkls:
			with open(fams_p+pkl_p, 'rb') as f:
				result = pickle.load(f)
				fams[d].append ( result.getROC() )
			f.close()
		fams[d].sort()
	median_roc = []
	for k, rocs in fams.items():
		total = len (rocs)
		#median_roc.append (rocs[total/2])
		median_roc.append (rocs[-1])
	y_axis = []
	x_axis = [j/100.0 for j in range (50, 101, 5)]
	for x in x_axis:
		_fam = 0
		for r in median_roc:
			if r >= x:
				_fam += 1
		y_axis.append(_fam)
	graph_data[file_p] = (x_axis, y_axis)
	acg = '{0} Amino Acid Groups'.format(file_p.split('.')[0])
	plt.plot(x_axis, y_axis, grh_l[i], label=acg)
	#out = open ('/tmp/'+file_p, 'wb')
	#pickle.dump(median_roc, out)
	#out.close()


x_axis = [j/100.0 for j in range (50, 101, 5)]
plt.plot(x_axis, [32, 30, 29, 27, 24, 20, 14, 6, 2, 2, 0], '-D', label='1000 Selected Featuers') #
#plt.plot(x_axis, [32, 30, 30, 29, 29, 22, 18, 7, 3, 2, 0], '-d', label='343 Selected Featuers') #
plt.plot(x_axis, [33, 33, 32, 32, 32, 30, 29, 24, 15, 8, 2], '-s', label='20 Amino Acid Groups') #
#plt.plot(x_axis, [33, 33, 33, 32, 32, 29, 27, 24, 20, 9, 1], '-.>', label='15 Amino Acid Groups') #
plt.plot(x_axis, [33, 32, 32, 32, 32, 31, 29, 27, 16, 10, 0], '-.*', label='10 Amino Acid Groups') #
#plt.plot(x_axis, [33, 33, 33, 32, 32, 29, 23, 15, 11, 9, 0], '--<', label='7 Amino Acid Groups') #
#plt.plot(x_axis, [33, 32, 32, 30, 28, 27, 22, 17, 9, 5, 0], '--o', label='4 Amino Acid Groups') #
plt.plot(x_axis, [33, 33, 33, 32, 31, 30, 29, 25, 19, 10, 0], '-^', label='Mismatch-SVM') # Theris
plt.legend(loc='lower left')
plt.grid()
plt.xlabel('AUC')
plt.ylabel('No. of families with given performance')
plt.axis([0.5,1,0,33])
plt.xticks ( np.arange(0.5, 1.01, 0.05) )
plt.show()

import os
import sys
import traceback
from threading import Thread
from subprocess import Popen, PIPE


ssh_workers = (
			['zombie'] * 5 +
			['lich'] * 5 +
			['shark'] * 5 +
			['skeleton'] * 5 +
			['ghoul'] * 5 +
			['ghast'] * 5 +
			['whale'] * 5 +
			['porpoise'] * 5 +
			['seal'] * 5 +
			['banshee'] * 1 +
			['bass'] * 5 +
			['butterfish'] * 5 +
			['bream'] * 5 +
			['chard'] * 5 +
			['clam'] * 5 +
			['conch'] * 5 +
			['walrus'] * 5 +
			['manatee'] * 5 +
			['mummy'] * 5 +
			['wraith'] * 5 +
			['kelp'] * 5 +
			['octopus'] * 5 +
			['eel'] * 5 +
			['pumpkin'] * 5 +
			['potato'] * 5 +
			['cockle'] * 5 +
			['seacucumber'] * 5 +
			['seapineapple'] * 5 +
			['crab'] * 5 +
			['crayfish'] * 5 +
			['cuttlefish'] * 5 +
			['blowfish'] * 5 +
			['bluefish'] * 5 +
			['celery'] * 5 +
			['pignut'] * 5 +
			['parsnip'] * 5 +
			['swede'] * 5 +
			['sweetpepper'] * 5 +
			['sprout'] * 5 +
			['spinach'] * 5 +
			['brill'] * 5 
			)
ssh_workers = (['iris-compute2']*16)

nr_local_worker = 15


class WorkerStopToken:  # used to notify the worker to stop
		pass


class Worker(Thread):

	def __init__(self,name,job_queue,result_queue):
		Thread.__init__(self)
		self.name = name
		self.job_queue = job_queue
		self.result_queue = result_queue

	def run(self):
		while True:
			cmdline = self.job_queue.get()
			if cmdline is WorkerStopToken:
				self.job_queue.put(cmdline)
				# print('worker {0} stop.'.format(self.name))
				break
			try:
				suc_run =  self.run_one(cmdline)
				if suc_run is None: raise RuntimeError("Run was not Successful")
			except:
				# we failed, let others do that and we just quit
				traceback.print_exception(sys.exc_info()[0], sys.exc_info()[1],
										  sys.exc_info()[2])
				self.job_queue.put(cmdline)
				print('worker {0} quit.'.format(self.name))
				break
			else:
				self.result_queue.put((self.name, suc_run))


class LocalWorker(Worker):

	def run_one(self, cmdline):
		result = Popen(cmdline,shell=True,stdout=PIPE).stdout
		for line in result.readlines():
			if str(line).find("FINISHED") != -1:
				return float(1)


class SSHWorker(Worker):

	def __init__(self,name,job_queue,result_queue,host):
		Worker.__init__(self,name,job_queue,result_queue)
		self.host = host
		self.cwd = os.getcwd()

	def run_one(self, cmdline):
		cmd_str = 'ssh -x {0} "cd {1}; {2}; echo FINISHED"'
		cmdline = cmd_str.format(self.host, self.cwd, cmdline)
		result = Popen(cmdline,shell=True,stdout=PIPE).stdout
		for line in result.readlines():
			if str(line).find("FINISHED") != -1:
				return float(1)

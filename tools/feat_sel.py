#!/usr/bin/python

from __future__ import division

from math import sqrt

import numpy as np
from PyML import SparseDataSet, svm


def rho(X, Y):
	""" X and Y are np arrays of features 
	"""
	numtr = sum((X-X.mean())*(Y-Y.mean()))
	dnumtr = sqrt(sum((X-X.mean())**2) * sum((Y-Y.mean())**2))
	return numtr/dnumtr

def rank_feat(feats, Y):
	ranks = []
	for X in feats:
		ranks.append(rho(X,Y))
	return ranks

if __name__ == '__main__':
	test_d = '/tmp/fisher_out/1.1.1.2/1.1.1.1:d1ash__.all_20.test'
	ds = SparseDataSet('/tmp/fisher_out/1.1.1.2/1.1.1.1:d1ash__.all_20.train')
	labels = np.array([int(n) for n in ds.labels.L])
	ranks = rank_feat(ds.getMatrix().T, labels)
	ranks = [(abs(r),i) for i, r in enumerate(ranks)]
	ranks.sort()
	ranks.reverse()
	feats = [f[1] for f in ranks]
	ds.keepFeatures(feats[:1000])
	ds.attachKernel('gaussian', gamma=0.2, C=2.0)
	s = svm.SVM()
	s.train(ds)
	test = SparseDataSet(test_d)
	test.keepFeatures(feats[:1000])
	results = s.test(test)
	import pdb; pdb.set_trace()

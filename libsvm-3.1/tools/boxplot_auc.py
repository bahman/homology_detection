#!/usr/bin/python
import matplotlib.pyplot as plt


def get_x_col(elem, *args):
	ret_arr = []
	for scheme in args:
		try:
			ret_arr.append(scheme[elem])		
		except IndexError:
			pass
			#ret_arr.append(0)
	return ret_arr

ab = [0.8593,0.7503,0.8173,0.9098,0.9142,0.9054,0.909,0.8266,0.7359]
dssp = [0.8346,0.8461,0.8588,0.8967,0.902,0.906,0.905]
gbmr = [0.5623,0.5964,0.7824,0.8517,0.872,0.883,0.904]
hsdm = [0.9128,0.6472,0.6964,0.8527,0.87,0.898,0.914,0.908]
lwi = [0.9128,0.8488,0.8603,0.9087,0.907,0.8773,0.85,0.783,0.745]
lwni = [0.9127,0.8488,0.8537,0.9085,0.909,0.883,0.844,0.783,0.745]
lzbl = [0.9087,0.8451,0.8558,0.9077,0.9072,0.908,0.908,0.828]
lzmj = [0.9234,0.7535,0.8193,0.901,0.906,0.899,0.903,0.795]
ml = [0.8675,0.8532,0.9121,0.9177,0.8885]
sdm = [0.9128,0.6632,0.7346,0.8527,0.9032,0.916]
td = [0.8593,0.7503,0.8173,0.9098,0.9142,0.9054,0.909]
lr = [0.9131]
all_g = [2,3,4,7,8,9,10,15,18]
g2 = get_x_col(0, ab, dssp, gbmr, hsdm, lwi, lwni, lzbl, lzmj, ml, sdm)
g3 = get_x_col(1, ab, dssp, gbmr, hsdm, lwi, lwni, lzbl, lzmj, ml, sdm)
g4 = get_x_col(2, ab, dssp, gbmr, hsdm, lwi, lwni, lzbl, lzmj, ml, sdm)
g7 = get_x_col(3, ab, dssp, gbmr, hsdm, lwi, lwni, lzbl, lzmj, ml, sdm)
g8 = get_x_col(4, ab, dssp, gbmr, hsdm, lwi, lwni, lzbl, lzmj, ml, sdm)
g9 = get_x_col(5, ab, dssp, gbmr, hsdm, lwi, lwni, lzbl, lzmj, ml, sdm)
g10 = get_x_col(6, ab, dssp, gbmr, hsdm, lwi, lwni, lzbl, lzmj, ml, sdm)
g15 = get_x_col(7, ab, dssp, gbmr, hsdm, lwi, lwni, lzbl, lzmj, ml, sdm)
g18 = get_x_col(8, ab, dssp, gbmr, hsdm, lwi, lwni, lzbl, lzmj, ml, sdm)
data = [g2, g3, g4, g7, g10, g15, g18]
x_axis = ['2','3','4','7','10','18','20']
plt.boxplot(data)
plt.show()


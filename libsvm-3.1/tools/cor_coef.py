import sys


def read_pred(res_p):
	try:
		res_f = open(res_p, 'r')
	except IOError:
		traceback.print_exception(sys.exc_info()[0], sys.exc_info()[1],
								  sys.exc_info()[2])
	pred_l = []
	actu_l = []
	dec_l = []
	for l in res_f:
		bl = l.split()
		if len(bl) == 2:
			if bl[0].find('dec_values') != -1:
				dec_l.append(float(bl[1]))
			elif bl[0].find('pred_results') != -1: 
					pred_l.append(float(bl[1]))	
			elif bl[0].find('Actual') != -1: 
					actu_l.append(float(bl[1]))	
	return pred_l, actu_l, dec_l


def get_tp_tn(pred_l, actu_l):
	tp_l = []
	tn_l = []
	tp1 = 0;
	fp1 = 0;
	tn0 = 0;
	fn0 = 0;
	for i, pred in enumerate(pred_l):
		if pred == actu_l[i]:
			if pred == 1:
				tp1 += 1
				tp_l.append(pred)
			elif pred == -1:
				tn0 += 1
				tn_l.append(pred)
		else:
			if pred == 1:
				fn0 += 1
			elif pred == -1:
				fp1 += 1
	p = tp1
	n = fp1
	o = tn0
	u = fn0
	cor = 0
	if ((p+o)*(p+u)*(n+o)*(n+u)) == 0:
		cor = 0
		#print 'tps', tp1, fp1, tn0, fn0, 'correlation',  (p*n - o*u)/1
	else:
		cor = (p*n - o*u)/((p+o)*(p+u)*(n+o)*(n+u))**0.5
		#print 'tps', tp1, fp1, tn0, fn0, 'correlation',  (p*n - o*u)/((p+o)*(p+u)*(n+o)*(n+u))**0.5
	return tp_l, tn_l, cor


if __name__ == '__main__':
	p_rocs = {}
	for p in sys.argv[1:]:
		pred_l, actu_l, dec_values = read_pred(p)
		#print len(pred_l), len(actu_l), len(dec_values)
		tp_l, tn_l, cor = get_tp_tn(pred_l, actu_l)
		#print len(tp_l), len(tn_l)
		print p, cor

'''
Created on 6 Aug 2011

@author: bahman
'''
#!/usr/bin/env python

import os
import sys
import traceback
import getpass
from time import sleep
from threading import Thread
from subprocess import Popen, PIPE

from ssh_connection import SSHConnection
from ssh_connection import ConnThreadStopToken
if(sys.hexversion < 0x03000000):
	import Queue
else:
	import queue as Queue


# svmtrain and gnuplot executable

is_win32 = (sys.platform == 'win32')
if not is_win32:
	svmtrain_exe = "../svm-train"
else:
	# example for windows
	svmtrain_exe = r"..\windows\svm-train.exe"

# global parameters and their default values

fold = 10

c_begin, c_end, c_step = -5, 15, 1
g_begin, g_end, g_step = 3, -15, -1
c_begin, c_end, c_step = -5, 15 , 1
g_begin, g_end, g_step = 0.1, 4, 0.05

global dataset_pathname, dataset_title, pass_through_string
global out_filename

# experimental

telnet_workers = []
ssh_workers = (
			['zombie'] * 7 +
			['lich'] * 7 +
			['shark'] * 7 +
			['skeleton'] * 7 +
			['ghoul'] * 7 +
			['whale'] * 7 +
			['porpoise'] * 7 +
			['seal'] * 7 +
			['banshee'] * 1 +
			['bass'] * 7 +
			['butterfish'] * 7 +
			['bream'] * 7 +
			['chard'] * 7 +
			['clam'] * 7 +
			['conch'] * 7 +
			['walrus'] * 7 +
			['manatee'] * 7 +
			['mummy'] * 7 +
			['wraith'] * 7 +
			['kelp'] * 7 +
			['octopus'] * 7 +
			['eel'] * 7 +
			['pumpkin'] * 7 +
			['potato'] * 7 +
			['cockle'] * 7 +
			['seacucumber'] * 7 +
			['seapineapple'] * 7 +
			['crab'] * 7 +
			['crayfish'] * 7 +
			['cuttlefish'] * 7 +
			['blowfish'] * 7 +
			['bluefish'] * 7 +
			['celery'] * 7 +
			['pignut'] * 7 +
			['parsnip'] * 7 +
			['swede'] * 7 +
			['sweetpepper'] * 7 +
			['sprout'] * 7 +
			['spinach'] * 7 +
			['brill'] * 7 
			)

nr_local_worker = 7

# process command line options, set global parameters
def process_options(argv=sys.argv):

	global fold
	global c_begin, c_end, c_step
	global g_begin, g_end, g_step
	global dataset_pathname, dataset_title, pass_through_string
	global svmtrain_exe, out_filename
	
	usage = """
		Usage: grid.py [-log2c begin,end,step] [-log2g begin,end,step] [-v fold] 
		[-svmtrain pathname] [-out pathname] [-png pathname]
		[additional parameters for svm-train] dataset
		"""

	if len(argv) < 2:
		print(usage)
		sys.exit(1)

	dataset_pathname = argv[-1]
	dataset_title = os.path.split(dataset_pathname)[1]
	out_filename = '{0}.out'.format(dataset_title)
	pass_through_options = []

	i = 1
	while i < len(argv) - 1:
		if argv[i] == "-log2c":
			i = i + 1
			(c_begin,c_end,c_step) = map(float,argv[i].split(","))
		elif argv[i] == "-log2g":
			i = i + 1
			(g_begin,g_end,g_step) = map(float,argv[i].split(","))
		elif argv[i] == "-v":
			i = i + 1
			fold = argv[i]
		elif argv[i] in ('-c','-g'):
			print("Option -c and -g are renamed.")
			print(usage)
			sys.exit(1)
		elif argv[i] == '-svmtrain':
			i = i + 1
			svmtrain_exe = argv[i]
		elif argv[i] == '-out':
			i = i + 1
			out_filename = argv[i]
		else:
			pass_through_options.append(argv[i])
		i = i + 1

	pass_through_string = " ".join(pass_through_options)
	assert os.path.exists(svmtrain_exe),"svm-train executable not found"	
	assert os.path.exists(dataset_pathname),"dataset not found"


def range_f(begin,end,step):
	''' Like range for floats as well
		
		Arguments:
		begin -- Begining of range
		end -- End of range
		step -- The step taken
		
	'''
	ret_range = []
	while True:
		if step > 0 and begin > end: break
		if step < 0 and begin < end: break
		ret_range.append(begin)
		begin = begin + step
	return ret_range

def permute_sequence(seq):
	n = len(seq)
	if n <= 1: return seq

	mid = int(n/2)
	left = permute_sequence(seq[:mid])
	right = permute_sequence(seq[mid+1:])

	ret = [seq[mid]]
	while left or right:
		if left: ret.append(left.pop(0))
		if right: ret.append(right.pop(0))

	return ret



def calculate_jobs():
	c_seq = permute_sequence(range_f(c_begin,c_end,c_step))
	g_seq = permute_sequence(range_f(g_begin,g_end,g_step))
	nr_c = float(len(c_seq))
	nr_g = float(len(g_seq))
	i = 0
	j = 0
	jobs = []

	while i < nr_c or j < nr_g:
		if i/nr_c < j/nr_g:
			# increase C resolution
			line = []
			for k in range(0,j):
				line.append((c_seq[i],g_seq[k]))
			i = i + 1
			jobs.append(line)
		else:
			# increase g resolution
			line = []
			for k in range(0,i):
				line.append((c_seq[k],g_seq[j]))
			j = j + 1
			jobs.append(line)
	return jobs

class WorkerStopToken:  # used to notify the worker to stop
		pass

class Worker(Thread):
	def __init__(self,name,job_queue,result_queue):
		Thread.__init__(self)
		self.name = name
		self.job_queue = job_queue
		self.result_queue = result_queue
	def run(self):
		while True:
			(cexp,gexp) = self.job_queue.get()
			if cexp is WorkerStopToken:
				self.job_queue.put((cexp,gexp))
				# print('worker {0} stop.'.format(self.name))
				break
			try:
				# TODO fix the g or c weather to do the power or not
				rate, tp, fp, tn, fn = self.run_one(2.0**cexp,gexp)
				if rate is None: raise RuntimeError("get no rate")
			except:
				# we failed, let others do that and we just quit
			
				traceback.print_exception(sys.exc_info()[0], sys.exc_info()[1],
										  sys.exc_info()[2])
				
				self.job_queue.put((cexp,gexp))
				print('worker {0} quit.'.format(self.name))
				break
			else:
				self.result_queue.put((self.name,cexp,gexp,rate,tp,fp,tn,fn))

class LocalWorker(Worker):
	def run_one(self,c,g):
		cmdline = ('{0} -c {1} -g {2} -v {3} {4} {5}'.format(svmtrain_exe, c,
						g, fold, pass_through_string, dataset_pathname))
		result = Popen(cmdline,shell=True,stdout=PIPE).stdout
		res_out = False	
		acc, tp, fp, tn, fn = -1, -1, -1, -1,-1
		for line in result.readlines():
			line = line.replace('\n', '')
			if res_out:
				if line.startswith("tp1"):
					tp = float(line.split()[-1])
				if line.startswith("fp1"):
					fp = float(line.split()[-1])
				if line.startswith("tn0"):
					tn = float(line.split()[-1])
				if line.startswith("fn0"):
					fn = float(line.split()[-1])
				if str(line).find("Cross") != -1:
					acc = float(line.split()[-1][0:-1])
					return acc, tp, fp, tn, fn
			else:
				if line.startswith("-----------------------"):
					res_out = True

class SSHWorker(Worker):
	def __init__(self,name,job_queue,result_queue,host):
		Worker.__init__(self,name,job_queue,result_queue)
		self.host = host
		self.cwd = os.getcwd()
	def run_one(self,c,g):
		cmd_str = 'ssh -x {0} "cd {1}; {2} -c {3} -g {4} -v {5} {6} {7}"'
		cmdline = cmd_str.format(self.host, self.cwd, svmtrain_exe, c, g, 
				fold, pass_through_string, dataset_pathname)
		result = Popen(cmdline,shell=True,stdout=PIPE).stdout
		res_out = False	
		acc, tp, fp, tn, fn = -1, -1, -1, -1,-1
		for line in result.readlines():
			if res_out:
				line = line.replace('\n', '')
				if line.startswith("tp1"):
					tp = float(line.split()[-1])
				if line.startswith("fp1"):
					fp = float(line.split()[-1])
				if line.startswith("tn0"):
					tn = float(line.split()[-1])
				if line.startswith("fn0"):
					fn = float(line.split()[-1])
				if str(line).find("Cross") != -1:
					acc = float(line.split()[-1][0:-1])
					return acc, tp, fp, tn, fn
			else:
				if line.startswith("-----------------------\n"):
					res_out = True


class TelnetWorker(Worker):
	def __init__(self,name,job_queue,result_queue,host,username,password):
		Worker.__init__(self,name,job_queue,result_queue)
		self.host = host
		self.username = username
		self.password = password		
	def run(self):
		import telnetlib
		self.tn = tn = telnetlib.Telnet(self.host)
		tn.read_until("login: ")
		tn.write(self.username + "\n")
		tn.read_until("Password: ")
		tn.write(self.password + "\n")

		# XXX: how to know whether login is successful?
		tn.read_until(self.username)
		# 
		print('login ok', self.host)
		tn.write("cd "+os.getcwd()+"\n")
		Worker.run(self)
		tn.write("exit\n")			   
	def run_one(self,c,g):
		cmd_str = '{0} -c {1} -g {2} -v {3} {4} {5}'
		cmdline = cmd_str.format(svmtrain_exe, c, g, fold, pass_through_string,
						dataset_pathname)
		self.tn.write(cmdline+'\n')
		(idx,matchm,output) = self.tn.expect(['Cross.*\n'])
		for line in output.split('\n'):
			if str(line).find("Cross") != -1:
				return float(line.split()[-1][0:-1])

def main():

	global ssh_workers
	# set parameters
	process_options()

	# put jobs in queue
	total_jobs = 0
	jobs = calculate_jobs()
	#for j in jobs:
		#for c,g in j:
			#print 2.0**c, g
	#exit()
	job_queue = Queue.Queue(0)
	result_queue = Queue.Queue(0)

	for line in jobs:
		for (c,g) in line:
			total_jobs += 1
			job_queue.put((c,g))
	# hack the queue to become a stack --
	# this is important when some thread
	# failed and re-put a job. It we still
	# use FIFO, the job will be put
	# into the end of the queue, and the graph
	# will only be updated in the end
 
	job_queue._put = job_queue.queue.appendleft


	# fire telnet workers

	if telnet_workers:
		nr_telnet_worker = len(telnet_workers)
		username = getpass.getuser()
		password = getpass.getpass()
		for host in telnet_workers:
			TelnetWorker(host,job_queue,result_queue,
					 host,username,password).start()
	"""
	#Connect to hosts first
	hostnames = set(ssh_workers) #removing the extras
	hostnames = list(hostnames)
	ssh_connector = SSHConnection(hostnames_l=hostnames)
	ssh_connector.conn_to_clients_wait()
	#con_names = [c.hostname for c in ssh_connector.con_clients_l] 
	connected_ones = [name.hostname  for name in ssh_connector.con_clients_l]
	ssh_workers = []
	for name in connected_ones:
		ssh_workers.append(name)
		ssh_workers.append(name)
		ssh_workers.append(name)
		ssh_workers.append(name)
		ssh_workers.append(name)
	print len(ssh_workers)
	
	# fire ssh workers
	if ssh_workers:
		for host in ssh_workers:
			SSHWorker(host,job_queue,result_queue,host).start()
	"""

	# fire local workers
	for i in range(nr_local_worker):
		LocalWorker('local' + str(i), job_queue, result_queue).start()

	# gather results

	done_jobs = {}


	result_f = open(out_filename, 'w')


	db = []
	best_rate = -1
	best_c1,best_g1 = None,None

	for line in jobs:
		for (c,g) in line:
			while (c, g) not in done_jobs:
				(worker,c1,g1,rate,tp,fp,tn,fn) = result_queue.get()
				done_jobs[(c1,g1)] = rate
				result_f.write('{0} {1} {2}\n'.format(c1,g1,rate))
				result_f.flush()
				if (rate > best_rate) or (rate==best_rate and g1==best_g1 and
												c1<best_c1):
					best_rate = rate
					best_c1,best_g1=c1,g1
					best_c = 2.0**c1
					best_g = g1
				nr_done_jobs = len(done_jobs)
				perc = nr_done_jobs * 100.0 / total_jobs
				perc = round(perc,3)
				state = str(nr_done_jobs)+" out of "+str(total_jobs)+" "+str(perc)+"%"
				res_str = ("[{0}] {1} {2} {3} tp={4} fp={5} tn={6} fn={7} "+
								"(best c={8}, g={9}, rate={10})")
				result = res_str.format(worker, 2**c1, g1, rate, tp,fp,tn,fn,
								best_c, best_g, best_rate) 
				print(result + " " + state)
			db.append((c,g,done_jobs[(c,g)]))

	job_queue.put((WorkerStopToken,None))
	print("{0} {1} {2}".format(best_c, best_g, best_rate))
	ssh_connector.close_clients()

if __name__ == '__main__':
	#sys.argv.append('../very_small.txt')
	#print sys.argv
	main()

import sys

if __name__ == '__main__':
	training = open(sys.argv[1], 'r')
	pred = open(sys.argv[2], 'r')
	wrong_ones = 'wrong_instance.libsvm'
	try:
		wrong_ons = sys.argv[3]
	except IndexError:
		pass
	wrong_ones_f = open(wrong_ones, 'w')
	t_arr, p_arr, training_set = [], [], []
	for l in training:
		training_set.append(l)
		t_arr.append(l.split()[0])
	for l in pred:
		p_arr.append(l.split()[0])
	cor, wrong, tn, tp = 0, 0, 0, 0
	total_p, total_n = 0, 0
	for i in range(len(t_arr)):
		if t_arr[i] == '1':
			total_p = total_p + 1
		if t_arr[i] == '-1':
			total_n = total_n + 1
		if t_arr[i] == p_arr[i]:
			if t_arr[i] == '-1':
				tn = tn + 1
			elif t_arr[i] == '1':
				tp = tp + 1
			cor = cor +1
		else:
			wrong += 1
			wrong_ones_f.write(training_set[i])
	print cor, wrong, 'out of', len(t_arr), float(cor)/len(t_arr)*100.0
	print tp, 'out of', total_p, '-', tn, 'out of', total_n

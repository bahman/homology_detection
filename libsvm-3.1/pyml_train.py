import sys
from PyML import *
from PyML import SparseDataSet

fold = 5

c = 128
g = 0.25


global dataset_title
global ds_path, out_filename

# process command line options, set global parameters
def process_options(argv=sys.argv):

	global fold
	global c, g
	global ds_path
	global dataset_title
	global out_filename

	usage = """
		Usage: mt_run.py [-g gamma] [-c cost][-v fold]
		[-svmtrain pathname] [-out pathname]
		[additional parameters for svm-train] -f datasets
		"""

	if len(argv) < 2:
		print(usage)
		sys.exit(1)

	pass_through_options = []
	out_filename = None

	ds_path = argv[-1]
	i = 1
	while i < len(argv):

		if argv[i] == "-v":
			i = i + 1
			fold = int(argv[i])
		elif argv[i] == '-c':
			i = i + 1
			c = float(argv[i])
		elif argv[i] == '-g':
			i = i + 1
			g = float(argv[i])
		elif argv[i] == '-out':
			i = i + 1
			out_filename = argv[i]
		else:
			pass_through_options.append(argv[i])
		i = i + 1





if __name__ == '__main__':
	process_options()
	print c,g, fold, ds_path
	data = SparseDataSet(ds_path)
	data.attachKernel('gaussian', gamma = g)
	s=SVM(C=c)
	r = s.cv(data, numFolds=fold)
	print "success rate:", r.getBalancedSuccessRate()
	print "area under ROC curve:", r.getROCn()
	print "confusion matrix -1,-1:", r.getConfusionMatrix()[0][0]
	print "confusion matrix +1,-1:", r.getConfusionMatrix()[0][1]
	print "confusion matrix -1,+1:", r.getConfusionMatrix()[1][0]
	print "confusion matrix +1,+1:", r.getConfusionMatrix()[1][1]
	print r
